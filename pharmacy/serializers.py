from rest_framework import serializers
from .models import Medicine, MedicineIssued
from patients.models import Register

class MedicineSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Medicine
        fields = '__all__'

class MedicineIssuedSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = MedicineIssued
        fields = '__all__'

class RegisterSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Register
        fields = '__all__'