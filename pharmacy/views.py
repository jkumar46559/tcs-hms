from django.shortcuts import render, redirect, HttpResponse
from .serializers import MedicineSerializer, MedicineIssuedSerializer
import requests
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .models import Medicine, MedicineIssued
from patients.models import Register
import json
from itertools import chain


# Create your views here.
def pharmacy(request):
    return render(request, 'pharmacy/pharmacy.html')

def searchpatient(request):
    patientid = int(request.GET['pid'])
    patient = Register.objects.filter(pid=patientid)
    medicine = MedicineIssued.objects.filter(patient=patient[0])
    #jsonobj = MedicineQuantIssuedSerializer(medicine[0].medicine_issued.all(), many=True)
    #print(jsonobj.data)
    #return HttpResponse(jsonobj.data)
    newset = chain(patient, medicine)
    data = serializers.serialize('json', newset)
    #mdata = serializers.serialize('json', medicine)
    #pdata = serializers.serialize('json', patient)
    return HttpResponse(data)
    '''
    context = {}
    if patient:
        context['name'] = patient[0].name
        context['age'] = patient[0].age
        context['address'] = patient[0].address
        context['date'] = str(patient[0].admission_date)
        context['bedtype'] = patient[0].bedtype
        context['medicine'] = data
    else:
        messages.error(request, 'Patient is not found !!!')
    response = json.dumps(context)
    return HttpResponse(response, content_type = "application/json") '''


def searchmedicine(request):
    name = str(request.GET['mname'])
    medicine = Medicine.objects.filter(name=name)
    context = {}
    found = False
    if medicine:
        found = True
        context['found'] = found
        context['rate'] = medicine[0].rate
    else:
        context['found'] = found
    response = json.dumps(context)
    return HttpResponse(response, content_type = "application/json")


def addmedicine(request):
    patientid = int(request.GET['pid'])
    patient = Register.objects.filter(pid=patientid)
    mname = str(request.GET['mname'])
    quantity = int(request.GET['quantity'])
    rate = int(request.GET['rate'])
    amount = int(request.GET['amount'])
    # Update the master medicine table
    medicine = Medicine.objects.filter(name=mname)
    MedicineIssued.updateMedicineTable(medicine=medicine[0], quantity=quantity)
    # Update the medicine issued table
    medicineobj = MedicineIssued.objects.filter(patient=patient[0], medicine=mname)
    if medicineobj:
        MedicineIssued.updateMedicineIssuedTable(imedicine=medicineobj[0], iquantity=quantity)
    else:
        medicine_issued = MedicineIssued(patient=patient[0], medicine=mname, quantity=quantity, rate=rate, amount=amount)
        medicine_issued.save()

    return HttpResponse("Success")