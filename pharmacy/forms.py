from django import forms
from patients.models import Register

class SearchForm(forms.ModelForm):
    class Meta:
        model = Register
        fields = ['ssn_id']