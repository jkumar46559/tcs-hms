from django.urls import path, include
from .views import pharmacy, searchpatient, searchmedicine, addmedicine

urlpatterns = [
    path('', pharmacy, name='pharmacy'),
    path('searchpatient/', searchpatient, name='search-patient'),
    path('searchmedicine/', searchmedicine, name='search-medicine'),
    path('addmedicine/', addmedicine, name='add-medicine'),
]