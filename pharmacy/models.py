from django.db import models
from patients.models import Register

# Create your models here.
class Medicine(models.Model):
    mid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    quant_avail = models.IntegerField()
    rate = models.IntegerField()

    def __str__(self):
        return self.name
        
class MedicineIssued(models.Model):
    patient = models.ForeignKey(Register, on_delete=models.CASCADE)
    medicine = models.CharField(max_length=50)
    quantity = models.IntegerField()
    rate = models.IntegerField()
    amount = models.IntegerField()

    @classmethod
    def updateMedicineTable(self, medicine, quantity):
        quant = medicine.quant_avail
        if quantity<=quant:
            quant = quant - quantity
        medicine.quant_avail = quant
        medicine.save(update_fields=['quant_avail'])

    @classmethod
    def updateMedicineIssuedTable(self, imedicine, iquantity):
        quant = imedicine.quantity
        quant = quant + iquantity
        imedicine.quantity = quant
        imedicine.amount = quant*imedicine.rate
        imedicine.save(update_fields=['quantity', 'amount'])