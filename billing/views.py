from django.shortcuts import render, redirect, HttpResponse
import requests
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from diagnostics.models import DiagnosticIssued
from pharmacy.models import MedicineIssued
from patients.models import Register
import json


# Create your views here.
@login_required
def onlinebilling(request):
    return render(request, 'billing/billing.html')

@login_required
def generatebill(request, pid):
    patient = Register.objects.filter(pid=pid)
    context = {}
    if patient:
        medicine = MedicineIssued.objects.filter(patient=patient[0])
        diagnostic = DiagnosticIssued.objects.filter(patient=patient[0])
        context['patient'] = patient[0]
        context['medicine'] = medicine
        context['diagnostic'] = diagnostic
        messages.success(request, 'Patient bill generated !!!')
    else:
        messages.warning(request, 'Patient not found !!!')
    
    return render(request, 'billing/generate.html', context=context)

@login_required
def discharge(request, pid):
    patient = Register.objects.filter(pid=pid)
    Register.changePatientStatus(patient=patient[0])
    return redirect('home')

