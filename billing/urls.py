from django.urls import path, include
from .views import onlinebilling, generatebill , discharge  

urlpatterns = [
    path('', onlinebilling, name='online-billing'),
    path('<int:pid>/generated/', generatebill, name='generate-bill'),
    path('<int:pid>/discharge/', discharge, name='discharge'),
]
