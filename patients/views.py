from django.shortcuts import render, redirect, HttpResponse
import requests
from django.contrib import messages
from .forms import PatientRegistration
from .models import Register
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def patienthome(request):
    patients = Register.objects.all()
    context = {
        'patients': patients,
    }
    return render(request, 'patients/patients.html', context=context)

@login_required
def patientlist(request):
    patients = Register.objects.all()
    context = {
        'patients': patients,
    }
    return render(request, 'patients/patientslist.html', context=context)

@login_required
def register(request):
    if request.method == 'post' or request.method == 'POST':
        form = PatientRegistration(request.POST)
        if form.is_valid():
            form.save()
            name = form.cleaned_data.get('name')
            messages.success(request, f'{name}, Patient creation initiated successfully.')
            return redirect('patients')
    else:
        form = PatientRegistration()
    return render(request, 'patients/register.html', {'form': form})

@login_required
def patientupdate(request, pid):
    patient = Register.objects.filter(pid=pid)
    if request.method == 'post' or request.method == 'POST':
        p_form = PatientRegistration(request.POST, instance=patient[0])
        if p_form.is_valid():
            p_form.save()
            messages.success(request, 'Patient updated Successfully !!!')
            return redirect('/patients/{}/view'.format(pid))
    else:
        p_form = PatientRegistration(instance=patient[0])

    context = {
        'p_form': p_form,
    }
    return render(request, 'patients/update.html', context)

@login_required
def patientview(request, pid):
    patient = Register.objects.filter(pid=pid)
    context = {
        'patient': patient[0],
    }
    return render(request, 'patients/view.html', context)

@login_required
def patientdelete(request, pid):
    patient = Register.objects.filter(pid=pid)
    if patient:
        patient[0].delete()
        messages.success(request, 'Patient removed Successfully !!!')
    else:
        messages.error(request, 'Patient not found !!!')
    return redirect('patients')