from django import forms
from .models import Register

class PatientRegistration(forms.ModelForm):
    class Meta:
        model = Register
        fields = ['ssn_id', 'name', 'age', 'admission_date', 'bedtype', 'address', 'city', 'state', 'status']