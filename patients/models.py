from django.db import models

# Create your models here.
BED_CHOICES = (
    ('general','General Ward'),
    ('sharing', 'Semi Sharing'),
    ('single','Single Room'),
)

STATUS = (
    ('active','Active'),
    ('discharge', 'Discharge'),
)

class Register(models.Model):
    pid = models.AutoField(primary_key=True)
    ssn_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=50)
    age = models.IntegerField()
    admission_date = models.DateField(auto_now=False, auto_now_add=False)
    bedtype = models.CharField(max_length=20, choices=BED_CHOICES, default='sharing')
    address = models.CharField("Address", max_length=1024)
    city = models.CharField("City", max_length=24)
    state = models.CharField("State", max_length=24)
    status = models.CharField(max_length=20, choices=STATUS, default='active')

    def __str__(self):
        return str(self.ssn_id) + "_" + str(self.name) 
        
    @classmethod
    def changePatientStatus(self, patient):
        status = patient.status
        if status=='active':
            status = 'discharge'
            patient.status = status
            patient.save(update_fields=['status'])

    
