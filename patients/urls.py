from django.urls import path, include
from .views import register, patientview, patientupdate, patientdelete, patienthome , patientlist

urlpatterns = [
    path('', patienthome, name='patients'),
    path('register/', register, name='patient-register'),
    path('list/', patientlist, name='patient-list'),
    path('<int:pid>/view', patientview, name='patient-view'),
    path('<int:pid>/update', patientupdate, name='patient-update'),
    path('<int:pid>/delete', patientdelete, name='patient-delete'),
]