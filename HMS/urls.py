from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('login.urls')),
    path('patients/', include('patients.urls')),
    path('pharmacy/', include('pharmacy.urls')),
    path('diagnostics/', include('diagnostics.urls')),
    path('search/',include('search.urls')),
    path('billing/', include('billing.urls')),
]
