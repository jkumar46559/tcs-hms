from django.contrib import admin
from .models import Diagnostic, DiagnosticIssued

# Register your models here.
admin.site.register(DiagnosticIssued)
admin.site.register(Diagnostic)
