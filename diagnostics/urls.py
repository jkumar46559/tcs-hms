from django.urls import path, include
from .views import diagnostics,searchpatient,searchdiagnostic,addDiagnostic
urlpatterns = [
    path('', diagnostics, name='diagnostics'),
    path('searchpatient/', searchpatient, name='search-patient-diag'),
    path('searchdiagnostic/', searchdiagnostic, name='search-diagnostic'),
    path('addDiagnostic/', addDiagnostic, name='add-diagnostic'),
]