from django.shortcuts import render, redirect, HttpResponse
import requests
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .models import Diagnostic, DiagnosticIssued
from patients.models import Register
import json
from itertools import chain

# Create your views here.
def diagnostics(request):
    return render(request, 'diagnostics/diagnostics.html')

def searchpatient(request):
    patientid = int(request.GET['pid'])
    patient = Register.objects.filter(pid=patientid)
    diagnostic = DiagnosticIssued.objects.filter(patient=patient[0])
    newset = chain(patient, diagnostic)
    data = serializers.serialize('json', newset)
    return HttpResponse(data)

def searchdiagnostic(request):
    name = str(request.GET['dname'])
    diagnostic = Diagnostic.objects.filter(name=name)
    context = {}
    found = False
    if diagnostic:
        found = True
        context['found'] = found
        context['amount'] = diagnostic[0].amount
    else:
        context['found'] = found
    response = json.dumps(context)
    return HttpResponse(response, content_type = "application/json")

def addDiagnostic(request):
    patientid = int(request.GET['pid'])
    patient = Register.objects.filter(pid=patientid)
    dname = str(request.GET['dname'])
    amount = int(request.GET['amount'])
    
    # Update the medicine issued table
    diagnosticobj = DiagnosticIssued.objects.filter(patient=patient[0], diagnostic=dname)
    if diagnosticobj:
        return HttpResponse("Already added")
    else:
        diagnostic_issued = DiagnosticIssued(patient=patient[0], diagnostic=dname, amount=amount)
        diagnostic_issued.save()

    return HttpResponse("Success")