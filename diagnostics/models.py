from django.db import models
from patients.models import Register

# Create your models here.
class Diagnostic(models.Model):
    did = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    amount = models.IntegerField()

    def __str__(self):
        return self.name

class DiagnosticIssued(models.Model):
    patient = models.ForeignKey(Register, on_delete=models.CASCADE)
    diagnostic = models.CharField(max_length=50)
    amount = models.IntegerField()