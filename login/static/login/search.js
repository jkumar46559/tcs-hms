/*
The Below code shows the js code for pharmacy.html page
.
.
.
.
.
.
.
.
.
*/
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/
//Disables Enter Key for patient id input element for pharmacy page(pharmacy.html)
$('#idd').keydown(function(e) {
    if(e.which == 13) { return false; }
});

//Executes when search icon in phramacy page is clicked
$('.search').click(function(e){
    e.preventDefault();
    $('#remove').empty();  
    $('#td1').empty();
    $('#td2').empty();
    $('#td3').empty();
    $('#td4').empty();
    $('#td5').empty(); 
    var patientId, href;
    href = $(this).attr("href");
    patientId = document.getElementById("idd").value;
    //Get patient data through ajax
    $.ajax({
        url: href,
        data: {
            pid: patientId
        },
        success:function(response){
            document.getElementById("issmedic").style.display = "block";
            data = jQuery.parseJSON(response);
            patient = data[0]['fields'];
            //Patient Details are printed
            $('#td1').text(patient["name"]);
            $('#td2').text(patient["age"]);
            $('#td3').text(patient["address"]);
            $('#td4').text(patient["admission_date"]);
            $('#td5').text(patient["bedtype"]);      
            var count = Object.keys(data).length; 
            $('#remove').empty();  
            //Medicine list is printed
            for (var i = 1 ; i < count ;i++)
            {
               medicine = data[i]['fields'];
               $('#remove').append(
                   `
                    <tr>
                    <td>${i}</td>
                    <td>${medicine["medicine"]}</td>
                    <td>${medicine["quantity"]}</td>
                    <td>${medicine["rate"]}</td>
                    <td>${medicine["amount"]}</td>
                    </tr>
                   `
               )
            }
        }
    });

});

//Executes when issue medicine button is clicked in pharmacy page (pharmacy.html)
$('#ambtn').click(function(e){
    document.getElementById("addmedic").style.display = "block";
});

//Executes when the add row button is clicked and adds a new row to the table by updating the previous record
$(document).ready(function(){
    var i=1;
    //Executes when delete row button of pharmacy page is clicked
    $("#delete_row").click(function(){
        if(i>1){
        $("#addr"+(i-1)).html('');
        i--;
        }
    });
    //Executes when add row button of pharmacy page is clicked
    $('#up').click(function(e){
    e.preventDefault();  
    var patientId, href, rate1;
    href1 = $(this).attr("href");
    href2 = $("#aa").attr("href");
    patientId = document.getElementById("idd").value;
    mname1 = document.getElementById("medic"+i).value;
    //Sends medicine name to the backend to verify whether the there is a medicine
     $.ajax({
        url:href2,
        data:   {
                mname : mname1,
                },
        success:function(response){
            if(response.found){
                rate1 = response.rate;
                $("#medir"+i).val(rate1);
                quantity1 = document.getElementById("mediq"+i).value;
                amount1 = rate1*quantity1;
                $("#media"+i).val(amount1);
                //After checking the availablility the data is uploaded to the database
                $.ajax(
                    {
                        url: href1,
                        data:   {
                                    pid: patientId,
                                    mname : mname1,
                                    quantity : quantity1,
                                    rate : rate1,
                                    amount : amount1
                                },
                        success:function(response)
                        {
                            alert("Medicine found and added successfully")

                            //Add new row to the table to provide an option to add another medicine
                            $('#addr'+i).html(
                                            `<td>
                                            `+(i+1)+`
                                            </td>
                                            <td>
                                                <input 
                                                    name='Medicine`+(i+1)+`' 
                                                    id='medic`+(i+1)+`' 
                                                    type='text' 
                                                    placeholder='Amedicine, Bmedicine, Cmedicine' 
                                                    class='form-control input-md'
                                                />
                                            </td>
                                            <td>
                                                <input  
                                                    name='Quantity`+(i+1)+`' 
                                                    id='mediq`+(i+1)+`' 
                                                    type='text' 
                                                    placeholder='Enter Quantity'  
                                                    class='form-control input-md'
                                                >
                                            </td>
                                            <td>
                                                <input  
                                                    name='Rate`+(i+1)+`' 
                                                    id='medir`+(i+1)+`' 
                                                    type='text' 
                                                    placeholder='Rate'  
                                                    class='form-control input-md' 
                                                    readonly
                                                >
                                            </td>
                                            <td>
                                                <input 
                                                    name='Amount`+(i+1)+`"' 
                                                    id='media`+(i+1)+`' 
                                                    type='text' 
                                                    placeholder='Amount' 
                                                    class='form-control input-md'  
                                                    readonly
                                                />
                                            </td>`
                                            );

                            $('#tab_logic').append(
                                `<tr 
                                    id="addr`+(i+1)+`">
                                </tr>`
                            );
                            i++; 
                            
                        }
                    });
            }
            else{
                alert("Medicine Not found! Please enter a valid medicine.")
            }
        }
    });
   });
});
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/


/*
The Below code shows the js code for diagnostic.html page
.
.
.
.
.
.
.
.
.
*/
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/
//Disables Enter Key for patient id input element for diagnostic page(diagnostic.html)
$('#iddd').keydown(function(e){
    if(e.which == 13) { return false; }
});

//Executes when search icon in diagnostics page is clicked
$('.searchd').click(function(e){
    e.preventDefault();
    $('#removed').empty();  
    $('#tdd1').empty();
    $('#tdd2').empty();
    $('#tdd3').empty();
    $('#tdd4').empty();
    $('#tdd5').empty();
    var patientId, href;
    href = $(this).attr("href");
    patientId = document.getElementById("iddd").value;

    //Get patient data through ajax
    $.ajax({
        url: href,
        data:   {
                pid: patientId
                },
        success:function(response)
        {
            document.getElementById("diags").style.display = "block";
            data = jQuery.parseJSON(response);
            patient = data[0]['fields'];

            //Patient Details are printed
            $('#tdd1').text(patient["name"]);
            $('#tdd2').text(patient["age"]);
            $('#tdd3').text(patient["address"]);
            $('#tdd4').text(patient["admission_date"]);
            $('#tdd5').text(patient["bedtype"]); 

            var count = Object.keys(data).length; 
            $('#removed').empty();  

            //Diagnostics Details are printed
            for (var i = 1 ; i < count ;i++)
            {
               diag = data[i]['fields'];
               $('#removed').append(
                   `
                    <tr>
                    <td>${i}</td>
                    <td>${diag["diagnostic"]}</td>
                    <td >${diag["amount"]}</td>
                    </tr>
                   `
               )
            }  
        }
    });

});

//Executes when add diagnostics button is clicked in pharmacy page (pharmacy.html)
$('#issdiag').click(function(e){
    document.getElementById("adddiag").style.display = "block";
});

//Executes when the add row button is clicked and adds a new row to the table by updating the previous record
$(document).ready(function(){
    var i=1;

    //Executes when delete row button of pharmacy page is clicked
    $("#delete_rowd").click(function()
        {
            if(i>1){
            $("#ddr"+(i-1)).html('');
            i--;
            }
        });

    //Executes when add row button of pharmacy page is clicked
    $('#upd').click(function(e)
    {
        e.preventDefault();  
        var patientId, href, rate1;
        href1 = $(this).attr("href");
        href2 = $("#aad").attr("href");
        patientId = document.getElementById("iddd").value;
        dname1 = document.getElementById("diag"+i).value;

        //Sends Diagnostic name to the backend to verify whether the there is a diagnostic
        $.ajax(
            {
                url:href2,
                data:   {
                        dname : dname1,
                        },
                success:function(response)
                {
                    if(response.found)
                        {
                        amount1 = response.amount;
                        $("#diaga"+i).val(amount1);

                        //After checking the valid diagnostic, the data is uploaded to the database
                        $.ajax(
                            {
                                url: href1,
                                data:   {
                                        pid: patientId,
                                        dname : dname1,
                                        amount : amount1
                                        },
                                success:function(response)
                                    {
                                        alert("Diagnostic found and added successfully")

                                        //Add new row to the table to provide an option to add another diagnostic
                                        $('#ddr'+i).html(
                                        `
                                        <td>
                                            `+(i+1)+`
                                        </td>

                                        <td>
                                            <input 
                                                name='Diagnostic`+(i+1)+`' 
                                                id='diag`+(i+1)+`' 
                                                type='text' placeholder='Xray, CT Scan, Dialysis, MRI Scan' 
                                                class='form-control input-md'
                                            />
                                        </td>

                                        <td>
                                            <input 
                                                name='Amount`+(i+1)+`' 
                                                id='diaga`+(i+1)+`' 
                                                type='text' 
                                                placeholder='Amount' 
                                                class='form-control input-md' 
                                                readonly
                                            />
                                        </td>`

                                        );

                                        $('#tab_logicd').append(
                                            `<tr 
                                                id="ddr`+(i+1)+`">
                                            </tr>
                                            `
                                        );
                                        i++; 
                                    }
                            });
                        }
                    else{
                        alert("Diagnostic Not found! Please enter a valid diagnostic.")
                    }
                }
            });

    });

});
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/


/*
The Below code shows the js code for search.html page
.
.
.
.
.
.
.
.
.
*/
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/
//Disables Enter Key for patient id input element for search patient page(search.html)
$('#ids').keydown(function(e) {
    if(e.which == 13) { return false; }
});

//This function redirects to the searched patient page after clicking the search button in search patient page  
function myFunc() { 
    var idf = document.getElementById("ids").value;   
    window.location.href="/search/"+idf; 
} 

/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/


/*
The Below code shows the js code for billing.html page
.
.
.
.
.
.
.
.
.
*/
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/
//This function redirects to the generated bill page after clicking the search button in billing patient page 
function myFunc1() { 
    var idf = document.getElementById("idb").value;   
    window.location.href=idf+"/generated/"; 
} 

/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/


/*
The Below code shows the js code for generate.html page
.
.
.
.
.
.
.
.
.
*/
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/
//Sums the amount column of pharmacy in generated bill page
$('#medb thead th').each(function(i) {
       calculateColumn(i);
});

function calculateColumn(index) {
    var total = 0;
    $('#medb tr').each(function() {
        var value = parseInt($('#t', this).eq(index).text());
        if ( !isNaN(value) ) {
            total += value;
            document.getElementById("bill2").innerHTML = "Bill for Pharmacy : " + "Rs."+total;
            tottt += value;
            document.getElementById("bill4").innerHTML = "Grant Total : " +"Rs."+tottt;
        }
    });
}

//Sums the amount column of diagnostic in generated bill page
$('#diagb thead th').each(function(i) {
    calculateColumnd(i);
});

function calculateColumnd(index) {
 var total = 0;
 $('#diagb tr').each(function() {
     var value = parseInt($('#d', this).eq(index).text());
     if ( !isNaN(value) ) {
         total += value;
         document.getElementById("bill3").innerHTML = "Bill for Diagnostics : " +"Rs."+ total;
         tottt += value;
         document.getElementById("bill4").innerHTML = "Grant Total : " +"Rs."+tottt;
     }
 });
}
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/


/*
The Below code shows the js code for patientlist.html page
.
.
.
.
.
.
.
.
.
*/
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/
//This fuction is used to show both active and discharged patients list
function allpat(){
    document.getElementById("all").style.display = "block";
    document.getElementById("active").style.display = "none";
}

//This fuction is used to show only active patients list 
function activepat() {
    document.getElementById("all").style.display = "none";
    document.getElementById("active").style.display = "block";

}
/*=============================xxxxxxxxxxxxxxxxxxxxxxxxxx==========================================*/