from django.urls import path, include
from .views import patientsearch, search

urlpatterns = [
    path('', search, name='pid-search'),
    path('<int:pid>/', patientsearch, name='patient-search'),
]