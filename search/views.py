from django.shortcuts import render, redirect, HttpResponse
import requests
from django.contrib import messages
from patients.models import Register
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def search(request):
    return render(request, 'search/search.html')

@login_required
def patientsearch(request, pid):
    patient = Register.objects.filter(pid=pid)
    context = {}
    found = False
    if patient:
        found = True
        context['patient'] = patient[0]
        messages.success(request, 'Patient searched successfully.')
    else:
        messages.warning(request, 'Patient not found.')
    context['found'] = found
    return render(request, 'search/result.html', context)